#include <iostream>
#include <string>
#include <stdlib.h> 
#include <time.h>
using namespace std;
#include "card.cpp"
#include "deck.cpp"
#include "WarPlayer.cpp"
#ifndef WARGAMES
#define WARGAMES

class WarGames{

private:
	WarPlayer* playerA;
	WarPlayer* playerB;
	deck* deckOfCards;
	int tieDeckCards;
	card* tieDeck[52];


public:


	WarGames(char* NameOfPlayerA, char* nameOfPlayerB){ //constructor
		playerA = new WarPlayer(NameOfPlayerA);
		playerB = new WarPlayer(nameOfPlayerB);
		deckOfCards = new deck();
		tieDeckCards = 0;

		for (int i = 0; i < 52; i++)
			tieDeck[i] = NULL;


		for (int i = 0; i < 26; i++){
			playerA->addLiveCard(deckOfCards->pickRandomCard());
			playerB->addLiveCard(deckOfCards->pickRandomCard());
		}


		playerA->printPlayer();
		playerB->printPlayer();

	}

	void printStatus(){
		cout << playerA->getName() << " have: " << playerA->getCardsValue() << " points." << endl;
		cout << playerB->getName() << " have: " << playerB->getCardsValue() << " points." << endl;

	}



	WarPlayer* startTheGame(int rounds){


		card* cardFromPlayerA;
		card* cardFromPlayerB;
		int numberOfRound = 1;

		while (rounds != 0){

			if (playerA->hasAnyCards() == false)//if one of the player have no cards, he lose
				return playerB;

			if (playerB->hasAnyCards() == false)//if the second player have no cards, he lose
				return playerA;



			cout << endl << "------------Round number: " << numberOfRound << " ------------" << endl;

			cardFromPlayerA = playerA->pickRandomCardFromLiveDeck();
			cardFromPlayerB = playerB->pickRandomCardFromLiveDeck();


			cout << playerA->getName() << "'s "; cardFromPlayerA->printCard();
			cout << endl;
			cout << playerB->getName() << "'s "; cardFromPlayerB->printCard();
			cout << endl;


			if (cardFromPlayerA->getValue() > cardFromPlayerB->getValue()){ //player A. value > player B. value
				playerA->addDeadCard(cardFromPlayerA);
				playerA->addDeadCard(cardFromPlayerB);

				cout << playerA->getName() << " wins the round" << endl;
			}

			if (cardFromPlayerA->getValue() < cardFromPlayerB->getValue()){ //player B. value > player A. value
				playerB->addDeadCard(cardFromPlayerA);
				playerB->addDeadCard(cardFromPlayerB);

				cout << playerB->getName() << " wins the round" << endl;
			}

			
			if (cardFromPlayerA->getValue() == cardFromPlayerB->getValue()){ // if have the same card - tie

				bool flag = true;
				int countCards = 0;

				// check if the card is the last card !
				if (playerA->hasAnyCards() == false){
					flag = false;
					playerB->addDeadCard(cardFromPlayerA);
					playerB->addDeadCard(cardFromPlayerB);
				}
				if (playerB->hasAnyCards() == false){
					flag = false;
					playerA->addDeadCard(cardFromPlayerA);
					playerA->addDeadCard(cardFromPlayerB);
				}


				while (flag == true){

					if (playerA->hasAnyCards() == false){//if  player A have no cards, he lose
						int j = 0;
						while (tieDeck[j] != NULL){
							playerB->addDeadCard(tieDeck[j]);
							tieDeck[j] = NULL;
							j++;
							tieDeckCards--;
						}
						cout << playerA->getName() << " cards value: " << playerA->getCardsValue() << " , " <<
							playerB->getName() << " cards value: " << playerB->getCardsValue() << endl << endl;
						cout << "Total cards: " << playerA->getName() << " - " << playerA->getNumberOfTotalCards() << " , " <<
							playerB->getName() << " - " << playerB->getNumberOfTotalCards() << endl;
						return playerB;
					}

					if (playerB->hasAnyCards() == false){//if the second player have no cards, he lose
						int j = 0;
						while (tieDeck[j] != NULL){
							playerA->addDeadCard(tieDeck[j]);
							tieDeck[j] = NULL;
							j++;
							tieDeckCards--;
						}
						cout << playerA->getName() << " cards value: " << playerA->getCardsValue() << " , " <<
							playerB->getName() << " cards value: " << playerB->getCardsValue() << endl << endl;
						cout << "Total cards: " << playerA->getName() << " - " << playerA->getNumberOfTotalCards() << " , " <<
							playerB->getName() << " - " << playerB->getNumberOfTotalCards() << endl;
						return playerA;
					}


					addTieCard(cardFromPlayerA);
					addTieCard(cardFromPlayerB);
					countCards++;
					countCards++;

					for (int i = 0; i < 3; i++){

						if (playerA->getNumberOfTotalCards() != 1){
							cardFromPlayerA = playerA->pickRandomCardFromLiveDeck();
							addTieCard(cardFromPlayerA);
							countCards++;
						}
						if (playerB->getNumberOfTotalCards() != 1){
							cardFromPlayerB = playerB->pickRandomCardFromLiveDeck();
							addTieCard(cardFromPlayerB);
							countCards++;
						}



					}

					//pick up the fourth cards or the last card.

					cardFromPlayerA = playerA->pickRandomCardFromLiveDeck();
					cardFromPlayerB = playerB->pickRandomCardFromLiveDeck();

					cout << "The fourth card's:" << endl;
					cout << playerA->getName() << "'s "; cardFromPlayerA->printCard();
					cout << endl;
					cout << playerB->getName() << "'s "; cardFromPlayerB->printCard();
					cout << endl;

					if (cardFromPlayerA->getValue() != cardFromPlayerB->getValue()){ //The fourth card - not tie
						flag = false;
						countCards++;
						countCards++;

						if (cardFromPlayerA->getValue() > cardFromPlayerB->getValue()){
							playerA->addDeadCard(cardFromPlayerA);
							playerA->addDeadCard(cardFromPlayerB);

							int j = 0;
							while (tieDeck[j] != NULL){ // if player A won - he get all the cards!
								playerA->addDeadCard(tieDeck[j]);
								tieDeck[j] = NULL;
								j++;
								tieDeckCards--;
							}

							cout << playerA->getName() << " wins the round - Earned " << countCards << " cards this round." << endl;
						}

						if (cardFromPlayerA->getValue() < cardFromPlayerB->getValue()){ 
							playerB->addDeadCard(cardFromPlayerA);
							playerB->addDeadCard(cardFromPlayerB);

							int j = 0;
							while (tieDeck[j] != NULL){ // if player B won - he get all the cards!
								playerB->addDeadCard(tieDeck[j]);
								tieDeck[j] = NULL;
								j++;
								tieDeckCards--;
							}

							cout << playerB->getName() << " wins the round - Earned " << countCards << " cards this round." << endl;
						}


					}



				} // end while


			} // end if - tie 


			// print the status of the game.
			cout << playerA->getName() << " cards value: " << playerA->getCardsValue() << " , " <<
				playerB->getName() << " cards value: " << playerB->getCardsValue() << endl << endl;
			cout << "Total cards: " << playerA->getName() << " - " << playerA->getNumberOfTotalCards() << " , " <<
				playerB->getName() << " - " << playerB->getNumberOfTotalCards() << endl;

			rounds--;
			numberOfRound++;

		}


		return playerA;
	}

	void addTieCard(card* card){

		tieDeck[tieDeckCards] = card;
		this->tieDeckCards++;
	}

	~WarGames(){
		if (playerA->getCardsValue() > playerB->getCardsValue()){
			cout << endl << endl << "************************************************" << endl
				<< "The Game is Over!" << endl;
			cout << "The winner of this game is: " << playerA->getName() << endl;
			cout << "************************************************" << endl;
		}
		if (playerA->getCardsValue() < playerB->getCardsValue()){
			cout << endl << endl << "************************************************" << endl
				<< "The Game is Over!" << endl;
			cout << "The winner of this game is: " << playerB->getName() << endl;
			cout << "************************************************" << endl;
		}
		if (playerA->getCardsValue() == playerB->getCardsValue()){
			cout << endl << endl << "************************************************" << endl
				<< "The Game is Over!" << endl;
			cout << "We have a tie! " << endl;
			cout << "************************************************" << endl;
		}
		cout << playerA->getName() << " deleted." << endl;
		cout << playerB->getName() << " deleted." << endl;
		delete playerA;
		delete playerB;
		delete deckOfCards;




	}
};






#endif;