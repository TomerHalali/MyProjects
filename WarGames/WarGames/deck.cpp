#include <iostream>
#include <string>
#include <stdlib.h>     /* srand, rand */
#include <time.h>
using namespace std;
#include "card.cpp"
#ifndef DECK
#define DECK

class deck{

private:

	card *deckOfCards[52];


public:

	deck(){ //constructor
		int numOfCard = 0;
		for (int value = 1; value < 14; value++)
			for (int type = 1; type < 5; type++){
				if (type == 1){
					deckOfCards[numOfCard] = new card(value, "diamond");
				}
				if (type == 2){
					deckOfCards[numOfCard] = new card(value, "Heart");
				}
				if (type == 3){
					deckOfCards[numOfCard] = new card(value, "Clover");
				}
				if (type == 4){
					deckOfCards[numOfCard] = new card(value, "Leaf");
				}
				numOfCard++;

			}
	}



	card* pickRandomCard(){

		srand(time(NULL));
		int pickOne = rand() % getSizeOfTheDeck(); //random card from 0 to the length of the deck (index)
		int lastCard = pickOne;
		card* temp = deckOfCards[pickOne];

		for (int i = pickOne; i < getSizeOfTheDeck()-1; i++){
			deckOfCards[i] = deckOfCards[i + 1];
			lastCard = i+1;
		}
		deckOfCards[lastCard] = NULL;
		return temp;




	}
	card* pickCardFromIndex(int cardIndex){
		card *tempCard = deckOfCards[cardIndex];

		int lastCard = cardIndex;

		if (cardIndex > getSizeOfTheDeck() - 1)
			return NULL;

		for (int i = cardIndex; i < getSizeOfTheDeck() - 1; i++){
			deckOfCards[i] = deckOfCards[i + 1];
			lastCard = i + 1;
		}
		deckOfCards[lastCard] = NULL;
		return tempCard;


	}
	
	int getSizeOfTheDeck(){
		int size = 0;
		for (int i = 0; i < 52; i++)
			if (deckOfCards[i] != NULL)
				size++;
		return size;

	}

	void printTheDeck(){
		for (int i = 0; i < getSizeOfTheDeck(); i++){
			deckOfCards[i]->printCard();
			cout << endl;
		}
	}

	~deck(){
		for (int i = 0; i < getSizeOfTheDeck(); i++)
			delete deckOfCards[i];
		cout << "Deck Deleted.";
	}


	

};

#endif;