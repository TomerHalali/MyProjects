#include <iostream>
#include <string>
#include <stdlib.h>     /* srand, rand */
#include <time.h>
using namespace std;
#include "card.cpp"
#ifndef WARPLAYER
#define WARPLAYER

class WarPlayer{

private:

	char* nameOfPlayer;
	card* liveDeck[52];
	card* deadDeck[52];
	int liveCards, deadCards;

public:

	
	WarPlayer(char* name){ //constructor
		nameOfPlayer = name;
		liveCards = 0;
		deadCards = 0;
	}

	char* getName(){
		return nameOfPlayer;
	}

	int getNumberOfLiveCards(){
		return liveCards;
	}
	int getNumberOfDeadCards(){
		return deadCards;
	}
	int getNumberOfTotalCards(){
		return deadCards+liveCards;
	}

	bool hasAnyCards(){
		if (liveCards == 0 && deadCards == 0)
			return false;
		return true;
	}
	void addLiveCard(card* card){

		liveDeck[liveCards] = card;
		this->liveCards++;
	}
	void addDeadCard(card* card){

		deadDeck[deadCards] = card;
		this->deadCards++;
	}

	void printPlayer(){
		cout << "My name is: " << nameOfPlayer << ". Value of all my cards is: " << getCardsValue() << " - " << getNumberOfTotalCards() <<" Cards." << endl;
	}

	int getCardsValue(){
		int sum = 0;
		for (int i = 0; deadDeck[i]!=NULL; i++)
			sum += deadDeck[i]->getValue();
		for (int i = 0; liveDeck[i] != NULL; i++)
			sum += liveDeck[i]->getValue();

		return sum;
	}


	card* pickRandomCardFromLiveDeck(){

		if (liveCards == 0){
			int i = 0;
			while (deadDeck[i]!=NULL)
			{
				liveDeck[i] = deadDeck[i];
				deadDeck[i] = NULL;
				deadCards--;
				liveCards++;

				i++;
			}


		}
		srand(time(NULL));
		int pickOne = rand() % liveCards; //random card from 0 to the length of the deck (index)

		card* temp = liveDeck[pickOne];
		int lastCard = pickOne;

		for (int i = pickOne; i < liveCards - 1; i++){
			liveDeck[i] = liveDeck[i + 1];
			lastCard = i + 1;
		}
		liveDeck[lastCard] = NULL;
		liveCards--;
		return temp;
	}


};



#endif;