/*Tomer Halali - ID 308203124 , Or Abitbol - ID 312614605*/
#include <iostream>
#include <string>
using namespace std;
#ifndef TICTACTOE
#define TICTACTOE


class Player //the player class
{
	friend class ticTacToe;
	friend class Point;

private:
	char* name;
	char mode;


public: //declaration all the functions

	void setName();
	void setMode(char a);
	char* getName();
	char getMode();
	Point* chooseMove();
	Player();
	~Player();
}; //end player class




class ticTacToe //the game class
{
	friend class Point;
	friend class Player;
private:
	int index = 0;
public: //declaration all the functions
	ticTacToe();
	char table[3][3];
	Player players[2];
	void printBoard();
	bool setBoard(Point* point,char a);
	bool gameOver(char[3][3],char mode);
	void winner(char* name);
	bool checkTie(char table[3][3]);
	bool isAvailable(int row, int col);

}; // end game class




class Point //a point class - help us to set Specific point in the board
{
	friend class ticTacToe;
	friend class Player;
private:
	int i, j;
public: //declaration all the functions
	bool setPoint(int i, int j); 
	int getI();
	int getJ();
	Point();

};// end point calss




#endif;