
#include "ticTacToe.h"
using namespace std;

Player::Player(){} // defulte constractur

Player::~Player() //distractue
{

	delete name;
}

void Player::setMode(char a) //set function (X or O)
{
	this->mode = a;
}

char Player::getMode() // get function (X or O)
{
	return this->mode;
}

char* Player::getName()// get function for the name
{
	return this->name;
}

Point* Player::chooseMove(){ // we got a move from the user
	char row, col;

	cout << "Select a row(1 to 3): " << endl;
	cin >> row; //get string from user (location one, location 2)
	while (row != '2' && row != '3' && row != '1') //we check first if it in the range
	{
		cout << "Out of range ! Please try again!" << endl;
		cout << "Select a row(1 to 3): " << endl;
		cin >> row;
	}
	cout << "Select a col(1 to 3): " << endl;
	cin >> col; //get string from user (location one, location 2)
	while (col != '2' && col != '3' && col != '1') //we check first if it in the range
	{
		cout << "Out of range ! Please try again!" << endl;
		cout << "Select a col(1 to 3): " << endl;
		cin >> col;
	}

	Point* point = new Point(); // new point object

	point->setPoint(row - '0' - 1, col - '0' - 1); //set new location with setPoint funtion

	return point; //return location
}


// Point Class Functions



bool Point::setPoint(int i, int j) //set the point location
{

	this->i = i; //put the row in the defulte
	this->j = j; //put the col in the defule
	return true;
}

int Point::getI() //get row function
{
	return this->i;
}

int Point::getJ(){ //get col function
	return this->j;
}


Point::Point(){}  // defulte constractur

	void Player::setName() // function to get a name from user
	{
		char tempName[20];
		int length;

		cin >> tempName; //Get name from user

		this->name = new char[strlen(tempName)]; //Allocate memory for player's name
		for (int i = 0; tempName[i]!='\0'; i++)
			this->name[i] = tempName[i]; //copy the user name to the object 'Player'
		this->name[strlen(tempName)] = '\0'; //the last char we put \0 

	}






	//TicTacToe functions

	void ticTacToe::winner(char* name) //function to tell who is the winner
	{
		cout << "Winner: " << name ; // massage to user with the name of the winner
	
	}

	 bool ticTacToe::checkTie(char table[3][3]){ // check function for tie between the players
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				if (table[i][j] == ' ')
					return false;
		return true; //check if all the board is full

	}

	 ticTacToe::ticTacToe(){ //TicTacToe constructor
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++){
				this->table[i][j] = ' ';
			}


		cout << "Enter the name of the first player(DO NO ENTER OVER THEN 20 LETTERS): ";
		this->players[0].setName(); // ask the names of the players
		cout << "Enter the name of the second player(DO NO ENTER OVER THEN 20 LETTERS): ";
		this->players[1].setName(); // ask the names of the players

		this->players[0].setMode('X'); // player number one allways X
		this->players[1].setMode('O');// Player number two allways O
		cout << players[0].getName() << " : X" << endl;
		cout << players[1].getName() << " : O" << endl;
	}
	 
	 void ticTacToe::printBoard(){ //we print the board with this funtion
		 int i, j, count = 0;

		 for (i = 0; i < 3; i++){

			 cout << "|";
			 for (j = 0; j < 3; j++){
				 if (table[i][j] == ' ')
					 cout << "   |";
				 if (table[i][j] == 'X')
					 cout << " X |";
				 if (table[i][j] == 'O')
					 cout << " O |";
			 }
			 if (i<2)
				 cout << endl << "-------------" << endl;
			 if (i == 2)
				 cout << endl;
		 }
	 }



	bool ticTacToe::setBoard(Point* point,char mode){ //function set up the table and upgrade the point X or O
		int i = point->getI(), j = point->getJ(); //the row and the col get fron the function get
		
		
		if (table[i][j] == 'X' || table[i][j] == 'O') // if its taken, return false
		{
			cout << "Spot taken. Choose a different one: " << endl; // send a massage to user
			return false;
		}

		table[i][j] = mode; // upgrade the table
		return true;
	}


	bool ticTacToe::gameOver(char table[3][3],char mode)// function to check if we have a winner
	{
		int diaCount=0, diagonalCount = 0,rowCount = 0,columnCount = 0; // all the optiones



		for (int i = 0; i < 3; i++) //Check main diagonal
			{
				if (table[i][i]==mode)
					diagonalCount++;
			}
		if (diagonalCount == 3) // if we have a match, we have a winner
			return true;



		for (int i = 2, j = 0; i >= 0; i--, j++)
		{
			if (mode == table[i][j]) //check the second diagonal
				diaCount++;
		}
		if (diaCount == 3) // if we have a match, we have a winner
			return true;
	



		for (int i = 0; i < 3; i++)
		{
			rowCount = 0;
			for (int j = 0; j < 3; j++) // we check aalll the rows
			{
				if (table[i][j] == mode)
					rowCount++;
			}
			if (rowCount == 3) // if we have a match, we have a winner
				return true;
		}




		for (int j = 0; j < 3; j++)
		{
			columnCount = 0;
			for (int i = 0; i < 3; i++) // we check all the columns
			{
				if (table[i][j] == mode)
					columnCount++;
			}
			if (columnCount == 3) // if we have a match, we have a winner
				return true;
		}

		return false; // dont have any match, the game is not finish
	}
	



